# Base

Este proyecto tiene todas la dependencias necesarias de php para desplegar Aspen.

Tiene una plantilla de php.ini_template donde se configuran las variables que van a ser reemplazadas por los valores de las varianble de entorno en tiempo de ejecución. 

Es deseable que en alguna futura versión no sea necesario tener que definir las variables acá, y que en tiempo de ejecución si exiten variables con un determinado prefijo ej: php_nombre_de_variable, se cree automáticamente la misma.

Para modificar dependecia y/o nuevas variables en el template seguir estos pasos:
- modificar los archivos necesarios
- git add -u
- git status
- git commit -m "mensaje del cambio" 
- git tag -a aa.bb.cc.dd (aa.bb.cc es numero tag de Mark y dd es numero correlativo de nuestros cambios empezando por 0)
- git push && git push origin aa.bb.cc.dd
